import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { AppService } from './app.service';

@Component({
  selector: 'my-app',
  template: `<button (click)="myInsert()">Click Me</button>`,
  providers: [AppService]
})
export class AppComponent  {
  value: string = "";

  constructor(private _AppService: AppService) { }

  myInsert() {
    if(this._AppService.insertData())
      alert('Data Inserted Successfully');
  }

 }