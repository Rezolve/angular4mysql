import { Injectable } from '@angular/core';
import { Http,  Response , RequestOptions, Headers} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AppService {
    postResponse: any ;
    constructor(private http: Http) { }

    insertData() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post('http://angular.test/mypage.php', JSON.stringify({firstName: 'Adrian', lastName: 'Ronan'}),{headers: headers}).map((res: Response) => res.json()).subscribe((res: '') => this.postResponse = res);
   }
}